import "./App.css";
import React, { useState, useMemo } from 'react';

function App() {

  const [number, setNumber] = useState('');
  const [valid, setValid] = useState(false);

  useMemo(() => isValid(number), [number])

  function handleChange(e) {
    setNumber(e.target.value);
  }

  function isValid(input) {
    const regex = new RegExp('^[0-9]');
    setValid(regex.test(input));
  }

  return (
    <div className="App">
      <div className="control has-icons-right">
        <input
          className="input is-large"
          type="text"
          placeholder="Enter number..."
          value={number}
          onChange={handleChange}
        />
        <span className="icon is-small is-right">
          <i className={valid ? "fas fa-check" : "fas fa-times"} />
        </span>
      </div>
    </div>
  );
}

export default App;
